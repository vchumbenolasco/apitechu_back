var user_file = require('../user.json');

//LOGIN
function login(req, res) {
  console.log('Login');
  console.log(req.body.email);
  console.log(req.body.password);
  let tam = user_file.length;
  let i=0;
  let encontrado = false;
  let idusuario = 0;
  while (i < user_file.length && !encontrado) {
    if (user_file[i].email==req.body.email && user_file[i].password ==req.body.password) {
        idusuario = user_file[i].id;
        encontrado = true;
        user_file[i].logged = true;
        writeUserDataToFile(user_file);
    }
    i++;
  }
  if (encontrado) {//encontrado
    res.send({"mensaje":"login correcto", "id":idusuario});
  }else{
    res.send({"mensaje":"login incorrecto"});
  }
  }
  //LOGOUT
function logout(req, res) {
  console.log('logout');
  let id = req.params.id;
  let encontrado = false;
  let i=0;
  console.log(id);
  while (i < user_file.length && !encontrado) {
    if (user_file[i].id==req.params.id && user_file[i].logged) {
        encontrado = true;
        delete user_file[i].logged;
        console.log('encontrado');
    }
    i++;
  }
  if (encontrado) {//encontrado
    res.send({"mensaje":"logout correcto", "id":id});
  }else{
    res.send({"mensaje":"logout incorrecto"});
  }
}

function writeUserDataToFile(data) {
     var fs = require('fs');
     var jsonUserData = JSON.stringify(data);
     fs.writeFile("./user.json", jsonUserData, "utf8",
      function(err) { //función manejadora para gestionar errores de escritura
        if(err) {
          console.log(err);
        } else {
          console.log("Datos escritos en 'users.json'.");
        }
      })
}


module.exports = {
      login,
      logout
  }
