var user_file = require('../user.json');

function getUsers(req, res){
  let pos = req.params.id -1;
  console.log("GET con id = " + req.params.id);
  let tam = user_file.length;
  console.log(tam);
  let respuesta = (user_file[pos] == undefined) ?
  {"msg":"Usuario no encontrado"}: user_file[pos];
  res.send(respuesta);
}
function createUsers(req, res){
    console.log('POST de users');
    let newID = user_file.length + 1;
    let newUser ={
      "id": newID,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    user_file.push(newUser);
    console.log("nuevo usuario:" + newUser);
    res.send(newUser);
}
module.exports.getUsers = getUsers;
module.exports.createUsers = createUsers;
