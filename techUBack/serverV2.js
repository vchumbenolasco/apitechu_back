var express = require('express');
var user_file = require('./user.json');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
const cors = require('cors');
var app = express();
var port = process.env.PORT||3000;
var URLbase = '/techu-peru/v3/';
var newID=0;

var baseMLabURL='https://api.mlab.com/api/1/databases/techu56db/collections/';
const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';

app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());

// GET users consumiendo API REST de mLab
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET /techu-peru/v3/users");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log(body.length);
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});
//Peticion GET users con ID
app.get(URLbase + 'users/:id',
  function (req, res) {
    console.log("GET /techu-peru/v3/users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":'+ id +'}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
    function(err, respuestaMLab, body) {
      console.log("Respuesta mLab correcta.");
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "Ningún elemento 'user'."}
          res.status(404);
        }
      }
      res.send(response);
    });
});

//Method GET with params MLab users and accounts with ID
app.get(URLbase +'users/:id/accounts',
 function(req,res){
   console.log('request=.params.id: '+req.params.id);
   var id=req.params.id;
   var queryString = 'q={"id_user":'+id+'}&';
   var clienteMlab=requestJSON.createClient(baseMLabURL);
   clienteMlab.get('account?'+queryString+apikeyMLab,
     function(error,requestMlab,body){
         console.log('error'+error);
         console.log('requestMlab'+requestMlab);
         console.log('body ' +body);
         res.send(body);
     });
 });

//POST 'user' mLab
app.post(URLbase + 'users',
  function (req, res) {
    console.log('POST de users');
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    console.log('Nuevo usuario: '+ req.body);
    clienteMlab.get('user?'+ apikeyMLab,
    function(error,requestMlab,body){
      let newID = body.length + 1;
      console.log('newID: '+ newID);
      let newUser ={
        "id": newID,
        "first_name":req.body.first_name,
        "last_name":req.body.last_name,
        "email":req.body.email,
        "password":req.body.password
        };
        clienteMlab.post(baseMLabURL +'user?'+ apikeyMLab, newUser,
          function(error,requestMlab,body){
            console.log(body);
            res.status(201);
            res.send(body);
          });
        });
      });

//PUT users con parámetro 'id'
app.put(URLbase + 'users/:id',
function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 var clienteMlab = requestJSON.createClient(baseMLabURL);
 clienteMlab.get('user?'+ queryStringID + apikeyMLab,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    clienteMlab.put(baseMLabURL +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
});

// Petición PUT con id de mLab (_id.$oid)
 app.put(URLbase + 'usersmLab/:id',
   function (req, res) {
     var id = req.params.id;
     let userBody = req.body;
     console.log('PUT con id de mLab (_id.$oid)');
     console.log(id);
     console.log(userBody);
     var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + apikeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "id" :parseInt(id),
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         // PUT a mLab
         httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 });

//DELETE user with id
app.delete(URLbase + "users/:id",
function(req, res){
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
 var httpClient = requestJSON.createClient(baseMLabURL);
 httpClient.get('user?' +  queryStringID + apikeyMLab,
   function(error, respuestaMLab, body){
     var respuesta = body[0];
     httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
       function(error, respuestaMLab,body){
         res.send(body);
     });
   });
});
 //DELETE user with id con PUT
app.delete(URLbase + "users/delete/:id",
function(req, res){
  console.log('DELETE user with id con PUT');
  var id = Number.parseInt(req.params.id);
  var queryStringID = 'q={"id":' + id + '}&';
  var query2 = 'q={id:${id} }&';
  var query3 = "q=" + JSON.stringify({"id":id}) + '&';
  console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
  var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.put(baseMLabURL + 'user?'+queryStringID+ apikeyMLab, [{}],
    function(error, respuestaMLab, body){
      var response = !error ? body:{
        "msg":"Error borrando usuario."
      }
      return res.send(response);
    });
});
//Method POST login
app.post(URLbase + 'login',
  function (req, res) {
    console.log('POST /techu-peru/v3/login');
    console.log(req.body.email);
    console.log(req.body.password);
    let email = req.body.email;
    let pass = req.body.password;
    let queryString  = 'q={"email":"' + email + '","password":"'+ pass +'"}&';
    let limFilter = 'l=1&';
    let clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?'+ queryString + limFilter + apikeyMLab,
      function(error, respuestaMLab, body) {
        if (!error) {
          console.log('user?'+ queryString + limFilter + apikeyMLab);
          if (body.length ==1) { //Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' +body[0].id + '}&' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut){
                res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id});
              });
          }else{
            res.status(404).send({'msg':'Usuario no valido'})
          }
        }
  });
});
//Method POST logout
app.post(URLbase + 'logout/:id',
  function (req, res) {
    console.log('POST /techu-peru/v3/logout/:id');
    console.log(req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"id":' + id + '}&';
    console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body) {
        if (!error) {
          if (body.length ==1) { //Existe un usuario que cumple 'queryString'
            let putBody = '{"$unset":{"logged":""}}';
            clienteMlab.put('user?q={"id": ' +body[0].id + '}&' + apikeyMLab, JSON.parse(putBody),
              function(errPut, resPut, bodyPut){
                res.send({'msg':'Logout correcto', 'user':body[0].email, 'userid':body[0].id});
              });
          }else{
            res.status(404).send({'msg':'Usuario no valido'})
          }
        }
  });
});

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
