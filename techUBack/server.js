var express = require('express');
var user_file = require('./user.json');
var bodyParser = require('body-parser');
const global = require('./global');
var app = express();
var port = process.env.PORT||3000;
const URL_BASE = '/techu-peru/v1/';
app.use(bodyParser.json());

const user_controller = require('./controllers/users.js');
const auth_controller = require('./controllers/auth.js');

// //Peticion GET users
// app.get(URL_BASE + 'users', function (req, res) {
// //  res.send({"msg":"Operacion Get exitosa"});
// res.status(201).send(user_file);
// });


//Peticion GET users con ID
app.get(URL_BASE + 'users/:id',
  function (req, res) {
    let pos = req.params.id -1;
    console.log("GET con id = " + req.params.id);
    let tam = user_file.length;
    console.log(tam);
    let respuesta = (user_file[pos] == undefined) ?
    {"msg":"Usuario no encontrado"}: user_file[pos];
    res.send(respuesta);
});

//GET users con Query String
app.get(URL_BASE+'users',
  function(req, res){
      console.log('GET con Query String');
      console.log(req.query.id);
      console.log(req.query.name);
});

//POST users
app.post(URL_BASE + 'users',
  function (req, res) {
    console.log('POST de users');
    // console.log('Nuevo usuario: '+ req.body);
    // console.log('Nuevo usuario: '+ req.body.first_name);
    // console.log('Nuevo usuario: '+ req.body.email);
    let newID = user_file.length + 1;
    let newUser ={
      "id": newID,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    user_file.push(newUser);
    console.log("nuevo usuario:" + newUser);
    res.send(newUser);


});

//PUT users
app.put(URL_BASE + 'users/:id',
  function (req, res) {
    console.log('PUT de users');
    let newUser ={
      "id": req.body.id,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    let pos = req.body.id - 1;
    user_file[pos] = newUser;
    console.log("nuevo usuario:" + newUser.first_name);
    res.send(newUser);


});

//DELETE users
app.delete(URL_BASE + 'users/:id',
  function (req, res) {
    console.log('Delete de users.');
    let pos = req.params.id -1;
    user_file.splice(pos,1);
    let tam = user_file.length;
    console.log(tam);
    if(user_file[pos] == undefined){
      res.send({"msg":"Usuario no existe..."});
    }else{
      user_file.splice(pos,1);
      res.send({"msg":"Eliminado Correctamente"});
    }
});

//login
app.post(URL_BASE + 'login',
  function (req, res) {
    console.log('Login');
    console.log(req.body.email);
    console.log(req.body.password);
    let tam = user_file.length;
    let i=0;
    let encontrado = false;
    let idusuario = 0;
    while (i < user_file.length && !encontrado) {
      if (user_file[i].email==req.body.email && user_file[i].password ==req.body.password) {
          idusuario = user_file[i].id;
          encontrado = true;
          user_file[i].logged = true;
          writeUserDataToFile(user_file);
      }
      i++;
    }
    if (encontrado) {//encontrado
      res.send({"mensaje":"login correcto", "id":idusuario});
    }else{
      res.send({"mensaje":"login incorrecto"});
    }
  });

//POST logout
// POST a /apitechu/v1/logout/:id
// Id es la id del usuario a deslogar.
// Tanto para logout correcto como incorrecto, devolver un mensaje informativo.
// En caso de logout correcto, guardar el cambio en el archivo.
// En el logout tenemos que quitar el campo logged del usuario (delete user.logged)
// Ejemplo de respuestas:
// Logout incorrecto { "mensaje" : "logout incorrecto" }
// Logout correcto { "mensaje" : "logout correcto", "idUsuario" : 1 }
// TEST
// · Login envío email + password correcto -> login correcto
// · envío email y/o password incorrecto -> login incorrecto
// · compruebo con un get de usuarios que un usuario logado tiene logged a true
// · Logout envío id de un usuario logado -> logout correcto
// · vuelvo a enviar la misma id de usuario -> logout incorrecto
// · compruebo con un get de usuarios que el usuario ya no tiene logged
// Ezequiel Llarena Borges [09:58]
// OPERACIONES OPCIONES API REST v1:
// [09:58]
// 1. Obtener todos los usuarios que estén logados. (OUTPUT = JSON con logados) (editado)
// [10:01]
// 2. Mediante una Query String limitar el numero de usuarios a mostrar.

//escritura en un archivo
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 };


app.post(URL_BASE + 'logout/:id',
  function (req, res) {
    console.log('logout');
    let id = req.params.id;
    let encontrado = false;
    let i=0;
    console.log(id);
    while (i < user_file.length && !encontrado) {
      if (user_file[i].id==req.params.id && user_file[i].logged) {
          encontrado = true;
          delete user_file[i].logged;
          console.log('encontrado');
      }
      i++;
    }
    if (encontrado) {//encontrado
      res.send({"mensaje":"logout correcto", "id":id});
    }else{
      res.send({"mensaje":"logout incorrecto"});
    }
});
//Obtener todos los usuarios que estén logados. (OUTPUT = JSON con logados)
app.post(URL_BASE + 'logged',
  function (req, res) {
    console.log('usuarios loggeados');
    var jsonUserData=[];
    for (var i = 0; i < user_file.length; i++) {
      if (user_file[i].logged== true) {
        jsonUserData.push(user_file[i]);
        console.log(jsonUserData);
      }
    }
    res.send(jsonUserData);
});
//Mediante una Query String limitar el numero de usuarios a mostrar.
app.get(URL_BASE + 'show',
  function (req, res) {
    console.log('mostrar usuarios loggeados');
    console.log(module.exports.URL_BASE);

    var jsonUserData=[];
    for (var i = 0; i < user_file.length; i++) {
      if(parseInt(jsonUserData.length) < parseInt(req.query.cantidad)){
        if (user_file[i].logged== true) {
          jsonUserData.push(user_file[i]);
        }
      }
    }
    res.send(jsonUserData);
});

app.get(global.URL_BASE + 'users/:id', user_controller.getUsers);

//login
app.post(global.URL_BASE + "login", auth_controller.login);
//logout
app.post(global.URL_BASE + "logout/:id", auth_controller.logout);

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
